crickquiz
=========

A Symfony project created on February 8, 2018, 4:20 pm.

## About ##
This project is created for Acutle.com to create some revenue
to get company running smoothly.
In case of company goes down the project will allow company in
harsh times.

### HOW TO USE THIS ###
- install composer
- Clone this git repo using _git clone https://vivekkushwaha39@bitbucket.org/vivekkushwaha39/crickquiz.git_
- Change directory to crickquiz folder.
- Open NetBeans and Select Create php project from existing source and select **crickquiz** directory.
