<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Entity;
use AppBundle\Entity\MatchInfo;
use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of User
 *
 * @author vivek
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="MatchInfo")
     * @var \Doctrine\Common\Collections\ArrayCollection 
     */
    private $matches;
    
    function getMatches()
    {
        return $this->matches;
    }

    function setMatches(\Doctrine\Common\Collections\ArrayCollection $matches)
    {
        $this->matches = $matches;
    }

        
    public function __construct()
    {
        parent::__construct();
        $this->matches = new \Doctrine\Common\Collections\ArrayCollection();
    }
}
