<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LeagueRecords
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class LeagueRecords
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_ended", type="datetime")
     */
    private $dateEnded;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateEnded
     *
     * @param \DateTime $dateEnded
     * @return LeagueRecords
     */
    public function setDateEnded($dateEnded)
    {
        $this->dateEnded = $dateEnded;

        return $this;
    }

    /**
     * Get dateEnded
     *
     * @return \DateTime 
     */
    public function getDateEnded()
    {
        return $this->dateEnded;
    }
}
