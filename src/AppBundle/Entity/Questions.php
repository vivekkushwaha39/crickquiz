<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\ObjectiveOptions;

/**
 * Questions
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Questions
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="question_text", type="string", length=500)
     */
    private $questionText;

    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="integer", options={"default":5})
     */
    private $points;

    /**
     * @var integer
     *
     * @ORM\Column(name="neg_points", type="integer", options={"default":0})
     */
    private $negPoints;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="extra_data", type="object", nullable=true)    
     */
    private $extraData;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection 
     * @ORM\ManyToMany(targetEntity="ObjectiveOptions", cascade={"persist"})
     * (name="questions_options", 
     * joinColumns={@ORM\JoinColumn(name="id", referencedColumnName="id", unique=true)},
     *                      
     *                  )
     */
    private $options;

    /**
     * @ORM\Column(name="qtype", type="integer")
     * @var integer 
     */
    private $qtype;

    public function __construct()
    {
        $this->options = new \Doctrine\Common\Collections\ArrayCollection();
        $this->negPoints = 0;
        $this->points = 0;
    }

    function getQtype()
    {
        return $this->qtype;
    }

    function setQtype($qtype)
    {
        $this->qtype = $qtype;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set questionText
     *
     * @param string $questionText
     * @return Questions
     */
    public function setQuestionText($questionText)
    {
        $this->questionText = $questionText;

        return $this;
    }

    /**
     * Get questionText
     *
     * @return string 
     */
    public function getQuestionText()
    {
        return $this->questionText;
    }

    /**
     * Set points
     *
     * @param integer $points
     * @return Questions
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer 
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set negPoints
     *
     * @param integer $negPoints
     * @return Questions
     */
    public function setNegPoints($negPoints)
    {
        $this->negPoints = $negPoints;

        return $this;
    }

    /**
     * Get negPoints
     *
     * @return integer 
     */
    public function getNegPoints()
    {
        return $this->negPoints;
    }

    /**
     * Set extraData
     *
     * @param \stdClass $extraData
     * @return Questions
     */
    public function setExtraData($extraData)
    {
        $this->extraData = $extraData;

        return $this;
    }

    /**
     * Get extraData
     * @return \stdClass 
     */
    public function getExtraData()
    {
        return $this->extraData;
    }

    public function toJson()
    {

        $opts = array();
        for ($i = 0; $i < $this->options->count(); $i++)
        {
            $opts[] = $this->options->get($i)->toJson();
        }

        return array(
            'id' => $this->id,
            'qtext' => $this->questionText,
            'points' => $this->points,
            'n_points' => $this->negPoints,
            'options' => $opts,
        );
    }

}
