<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Player
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Player
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="player_name", type="string", length=255)
     */
    private $playerName;

    /**
     * @var integer
     *
     * @ORM\Column(name="strength", type="integer")
     */
    private $strength;

    /**
     * @var integer
     *
     * @ORM\Column(name="strength_point", type="integer")
     */
    private $strengthPoint;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_playing", type="boolean")
     */
    private $isPlaying;

    /**
     *@ORM\ManyToMany(targetEntity="Player", mappedBy="players")
     */
    private $team;

    /**
     *
     * @var integer 
     * @ORM\Column(name="point_3n9", type="integer")
     */
    private $point3n9;
    
    /**
     *
     * @var integer 
     * @ORM\Column(name="point_4", type="integer")
     */
    private $point4;
    
    
    /**
     *
     * @var integer 
     * @ORM\Column(name="point_6", type="integer")
     */
    private $point6;
    
    
    
    /**
     *
     * @var integer 
     * @ORM\Column(name="point_7", type="integer")
     */
    private $point7;
    
    /**
     *
     * @var integer 
     * @ORM\Column(name="point_8", type="integer")
     */
    private $point8;
    
    
    
    /**
     *
     * @var integer 
     * @ORM\Column(name="point_16", type="integer")
     */
    private $point16;
    
    
    
    /**
     *
     * @var integer 
     * @ORM\Column(name="point_17", type="integer")
     */
    private $point17;
    
    
    /**
     *
     * @var integer 
     * @ORM\Column(name="point_18", type="integer")
     */
    private $point18;
    
    
    function getPoint8()
    {
        return $this->point8;
    }

    function setPoint8($point8)
    {
        $this->point8 = $point8;
    }

        
    function getPoint3n9()
    {
        return $this->point3n9;
    }

    function getPoint4()
    {
        return $this->point4;
    }

    function getPoint6()
    {
        return $this->point6;
    }

    function getPoint7()
    {
        return $this->point7;
    }

    function getPoint16()
    {
        return $this->point16;
    }

    function getPoint17()
    {
        return $this->point17;
    }

    function getPoint18()
    {
        return $this->point18;
    }

    function setPoint3n9($point3n9)
    {
        $this->point3n9 = $point3n9;
    }

    function setPoint4($point4)
    {
        $this->point4 = $point4;
    }

    function setPoint6($point6)
    {
        $this->point6 = $point6;
    }

    function setPoint7($point7)
    {
        $this->point7 = $point7;
    }

    function setPoint16($point16)
    {
        $this->point16 = $point16;
    }

    function setPoint17($point17)
    {
        $this->point17 = $point17;
    }

    function setPoint18($point18)
    {
        $this->point18 = $point18;
    }

        
    
    
    /**
     * Get Team data
     * @return type
     */
    public function getTeam() 
    {
        return $this->team;
    }

    /**
     * Set team Members
     * @param type $team
     */
    public function setTeam($team)
    {
        $this->team = $team;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set playerName
     *
     * @param string $playerName
     * @return Player
     */
    public function setPlayerName($playerName)
    {
        $this->playerName = $playerName;

        return $this;
    }

    /**
     * Get playerName
     *
     * @return string 
     */
    public function getPlayerName()
    {
        return $this->playerName;
    }

    /**
     * Set strength
     *
     * @param integer $strength
     * @return Player
     */
    public function setStrength($strength)
    {
        $this->strength = $strength;

        return $this;
    }

    /**
     * Get strength
     *
     * @return integer 
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * Set strengthPoint
     *
     * @param integer $strengthPoint
     * @return Player
     */
    public function setStrengthPoint($strengthPoint)
    {
        $this->strengthPoint = $strengthPoint;

        return $this;
    }

    /**
     * Get strengthPoint
     *
     * @return integer 
     */
    public function getStrengthPoint()
    {
        return $this->strengthPoint;
    }

    /**
     * Set isPlaying
     *
     * @param boolean $isPlaying
     * @return Player
     */
    public function setIsPlaying($isPlaying)
    {
        $this->isPlaying = $isPlaying;

        return $this;
    }

    /**
     * Get isPlaying
     *
     * @return boolean 
     */
    public function getIsPlaying()
    {
        return $this->isPlaying;
    }
    
    public function __construct() 
    {
        $this->team = new \Doctrine\Common\Collections\ArrayCollection();
    }
}
