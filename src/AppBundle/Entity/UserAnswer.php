<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAnswer
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class UserAnswer
{
    
    /**
     * @ORM\Column(name="match_id", type="integer")
     * @var int 
     */
    private $matchId;
    
    /**
     * @ORM\Column(name="user_id", type="integer")
     * @var integer 
     */
    private $userId;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="q_no", type="integer")
     */
    private $qNo;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer", type="integer")
     */
    private $answer;
    
    
    
    function getUserId()
    {
        return $this->userId;
    }

    function setUserId($userId)
    {
        $this->userId = $userId;
    }

        

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qNo
     *
     * @param integer $qNo
     * @return UserAnswer
     */
    public function setQNo($qNo)
    {
        $this->qNo = $qNo;

        return $this;
    }

    /**
     * Get qNo
     *
     * @return integer 
     */
    public function getQNo()
    {
        return $this->qNo;
    }

    /**
     * Set answer
     *
     * @param integer $answer
     * @return UserAnswer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return integer 
     */
    public function getAnswer()
    {
        return $this->answer;
    }
    
    
    public function getMatchId()
    {
        return $this->matchId;
    }

    public function setMatchId($matchId)
    {
        $this->matchId = $matchId;
    }


}
