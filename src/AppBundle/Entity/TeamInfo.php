<?php

namespace AppBundle\Entity;
define('STRENGTH_BATSMAN', 1);
define('STRENGTH_BOWLER', 2);
define('STRENGTH_BATWICKET', 3);
define('STRENGTH_ALL_ROUNDER', 4);


use Doctrine\ORM\Mapping as ORM;

/**
 * TeamInfo
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class TeamInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="team_name", type="string", length=255)
     */
    private $teamName;

    /**
     * @var integer
     *
     * @ORM\Column(name="rating", type="integer")
     */
    private $rating;

    
    /**
     * @ORM\ManyToMany(targetEntity="Player")
     */
    private $players;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="point_1n12n13", type="integer")
     */
    private $point1n12n13;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="point_2", type="integer")
     */
    private $point2;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="point_5", type="integer")
     */
    private $point5;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="point_10", type="integer")
     */
    private $point10;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="point_11", type="integer")
     */
    private $point11;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="point_14", type="integer")
     */
    private $point14;
    /**
     *
     * @var string
     * @ORM\Column(name="team_col", type="string") 
     */
    private $teamCol;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="point_15", type="integer")
     */
    private $point15;
    
    public function getPoint1n12n13()
    {
        return $this->point1n12n13;
    }

    public function getPoint2()
    {
        return $this->point2;
    }

    public function getPoint5()
    {
        return $this->point5;
    }

    public function getPoint10()
    {
        return $this->point10;
    }

    public function getPoint11()
    {
        return $this->point11;
    }

    public function getPoint14()
    {
        return $this->point14;
    }

    public function getPoint15()
    {
        return $this->point15;
    }

    function setPoint1n12n13($point1n12n13)
    {
        $this->point1n12n13 = $point1n12n13;
    }

    public function setPoint2($point2)
    {
        $this->point2 = $point2;
    }

    public function setPoint5($point5)
    {
        $this->point5 = $point5;
    }

    public function setPoint10($point10)
    {
        $this->point10 = $point10;
    }

    public function setPoint11($point11)
    {
        $this->point11 = $point11;
    }

    public function setPoint14($point14)
    {
        $this->point14 = $point14;
    }

    public function setPoint15($point15)
    {
        $this->point15 = $point15;
    }
    
    
    public function getPlayers() {
        return $this->players;
    }
    
    public function setPlayers($players) {
        $this->players = $players;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set teamName
     *
     * @param string $teamName
     * @return TeamInfo
     */
    public function setTeamName($teamName)
    {
        $this->teamName = $teamName;

        return $this;
    }

    /**
     * Get teamName
     *
     * @return string 
     */
    public function getTeamName()
    {
        return $this->teamName;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return TeamInfo
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }
    
    public function getTeamCol()
    {
        return $this->teamCol;
    }

    public function setTeamCol($teamCol)
    {
        $this->teamCol = $teamCol;
    }

        
    public function __construct() {
        $this->players = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function toJson()
    {
        return array(
            'id'  => $this->id,
            'team_name' => $this->teamName,
            'rating' => $this->rating,
        );
        
    }
    
}
