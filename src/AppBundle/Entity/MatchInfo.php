<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MatchInfo
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MatchInfo
{

    public static $STATUS_PENDING = 1;
    public static $STATUS_RESULT_OUT = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="match_name", type="string", length=255)
     */
    private $matchName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime")
     */
    private $createdOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_time", type="datetime")
     */
    private $endTime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_published", type="boolean")
     */
    private $isPublished;

    /**
     * @ORM\ManyToMany(targetEntity="TeamInfo", cascade={"persist"})
     * @ORM\JoinTable(name="match_teama")
     */
    private $teamA;

    /**
     * @ORM\ManyToMany(targetEntity="TeamInfo", cascade={"persist"})
     * @ORM\JoinTable(name="match_teamb")
     */
    private $teamB;

    /**
     * @ORM\Column(name="match_place")
     * @var string 
     */
    private $matchPlace;

    /**
     * 
     * @ORM\ManyToMany(targetEntity="Questions", cascade={"persist"})
     */
    private $questions;

    /**
     * @ORM\Column(name="match_time")
     * @var \DateTime 
     */
    private $matchTime;

    /**
     * @var integer
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getMatchTime()
    {
        return $this->matchTime;
    }

    public function setMatchTime(\DateTime $matchTime)
    {
        $this->matchTime = $matchTime;
    }

    public function getMatchPlace()
    {
        return $this->matchPlace;
    }

    public function setMatchPlace($matchPlace)
    {
        $this->matchPlace = $matchPlace;
    }

    public function getQuestions()
    {
        return $this->questions;
    }

    public function setQuestions($questions)
    {
        $this->questions = $questions;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set matchName
     *
     * @param string $matchName
     * @return MatchInfo
     */
    public function setMatchName($matchName)
    {
        $this->matchName = $matchName;

        return $this;
    }

    /**
     * Get matchName
     *
     * @return string 
     */
    public function getMatchName()
    {
        return $this->matchName;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return MatchInfo
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     * @return MatchInfo
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime 
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     * @return MatchInfo
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean 
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    public function getTeamA()
    {
        return $this->teamA;
    }

    public function getTeamB()
    {
        return $this->teamB;
    }

    public function setTeamA(\Doctrine\Common\Collections\ArrayCollection $teamA)
    {
        $this->teamA = $teamA;
    }

    public function setTeamB(\Doctrine\Common\Collections\ArrayCollection $teamB)
    {
        $this->teamB = $teamB;
    }

    public function __construct($matchName, $endTime, $createdOn, $ispublished)
    {
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdOn = $createdOn;
        $this->endTime = $endTime;
        $this->isPublished = $ispublished;
        $this->teamA = new \Doctrine\Common\Collections\ArrayCollection();
        $this->teamB = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function toJson()
    {

        $ta = $this->getTeamA()->get(0)->toJson();
        $tb = $this->getTeamB()->get(0)->toJson();
        return array(
            'id' => $this->id,
            'match_name' => $this->matchName,
            'end_time' => $this->endTime,
            'start_time' => $this->createdOn,
            'team_a' => $ta,
            'team_b' => $tb,
        );
    }

}
