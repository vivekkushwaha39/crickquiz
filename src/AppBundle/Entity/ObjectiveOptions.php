<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ObjectiveOptions
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ObjectiveOptions
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="option_value", type="string", length=255)
     */
    private $optionValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="extra_points", type="integer")
     */
    private $extraPoints;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set optionValue
     *
     * @param string $optionValue
     * @return ObjectiveOptions
     */
    public function setOptionValue($optionValue)
    {
        $this->optionValue = $optionValue;

        return $this;
    }

    /**
     * Get optionValue
     *
     * @return string 
     */
    public function getOptionValue()
    {
        return $this->optionValue;
    }

    /**
     * Set extraPoints
     *
     * @param integer $extraPoints
     * @return ObjectiveOptions
     */
    public function setExtraPoints($extraPoints)
    {
        $this->extraPoints = $extraPoints;

        return $this;
    }

    /**
     * Get extraPoints
     *
     * @return integer 
     */
    public function getExtraPoints()
    {
        return $this->extraPoints;
    }

    public function __construct($optionValue, $extraPonts)
    {
        $this->optionValue = $optionValue;
        $this->extraPoints = $extraPonts;
    }

    public function toJson()
    {
        return array(
            'id' =>$this->id,
            'option_value' => $this->optionValue,
            
        );
    }
}
