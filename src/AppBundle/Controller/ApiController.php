<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\MatchInfo;
/**
 * Description of ApiController
 *
 * @author vivek
 */
class ApiController extends Controller{
    
    
    /**
     * @Route("/api/match/add", name="add-match")
     * @param Request $req
     */
    public function insertQuestion(Request $req){
        
        $match = new MatchInfo();
        $match->setMatchName('A vs B');
        $match->setIsPublished(false);
        $match->setCreatedOn(new \DateTime('2015-10-10 12:12:12'));
        $match->setEndTime(new \DateTime('2015-10-10 12:12:12'));
        
        $session =  $this->getDoctrine()->getEntityManager();
//        $session->persist($match);
//        $session->flush();
        $res = $session->createQuery('select * from MatchInfo')->execute();
        return  new Response('Inserted');
    }
    
    /**
     *  @Route("/api", name="api-home")
     */
    public function apiIndexAction(Request $req){
        return new Response('hi');
    }
    
    /**
     *  @Route("/api/addsome")
     */
    
    public function addSomeData(Request $request) {
        $match = new MatchInfo('A vs B',new \DateTime('2015-10-10 12:12:12') , new \DateTime('2015-10-10 12:12:12') , true);
        
        $quests  = new \Doctrine\Common\Collections\ArrayCollection();
        
        $question1 = new \AppBundle\Entity\Questions();
        $question1->setQuestionText("What is the first letter of english alphabet");
        $quests->add($question1);
        
        $options = new \Doctrine\Common\Collections\ArrayCollection();
        $opt1 = new \AppBundle\Entity\ObjectiveOptions('A', 10);
        $opt2 = new \AppBundle\Entity\ObjectiveOptions('B',10);
        $opt3 = new \AppBundle\Entity\ObjectiveOptions('C',10);
        
        $options->add($opt1);
        $options->add($opt2);
        $options->add($opt3);
        
        $question1->setOptions($options);
        $match->setQuestions($quests);
        
        
        
        
        $session =  $this->getDoctrine()->getEntityManager();
        $session->persist($match);
        $session->flush();
//        $res = $session->createQuery('select * from MatchInfo')->execute();
        return  new Response('Inserted');
        
    }
    
    
}
