<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\MatchInfo;
use AppBundle\Controller\LoginController;
use AppBundle\Controller\CorsResponse;

define('MATCH_ID', 'match_id');

class MatchController extends Controller
{

    /**
     * @Route("/api/match-info")
     * @Template()
     */
    public function getMatchAction(Request $request)
    {

        $matchId = $request->request->getInt(MATCH_ID, 0);


        if ($matchId == 0)
        {
            return $this->getAllMatch($request);
        }
        $entityManager = $this->getDoctrine()->getEntityManager();
        
        $query = $entityManager->createQuery(
                'select m from'
                .' AppBundle\Entity\MatchInfo m '
                . ' where m.createdOn < :today '
                . ' and m.endTime > :today '
                . ' and m.isPublished = 1 '
                . ' and m.id = :givenId '
        )->setParameter('today', new \DateTime())->setParameter('givenId', $matchId);
        
        $repo = $this->getDoctrine()->getRepository(MatchInfo::class);

        /* @var $match MatchInfo */
        $match = $query->execute();

        if (count($match) < 1)
        {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
        }

        $matchJson = $match[0]->toJson();


        return new CorsResponse($matchJson);
    }

    /**
     * @Route("/api/questions")
     * @param Request $request
     * @throws \Symfony\Component\HttpKernel\Exception\LengthRequiredHttpException
     */
    public function questionAction(Request $request)
    {
        $matchId = $request->get("match_id");

        if ($matchId == null)
        {
            throw new \Symfony\Component\HttpKernel\Exception\LengthRequiredHttpException();
        }

        $match_session = $this->getDoctrine()->getRepository(MatchInfo::class);

        /* @var $match MatchInfo */
        $match = $match_session->find($matchId);

        if (!$match)
        {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No matches found');
        }

        /* @var $questions \Doctrine\Common\Collections\ArrayCollection */
        $questions = $match->getQuestions();

        $quests_array = array();

        for ($i = 0; $i < $questions->count(); $i++)
        {
            /* @var $question \AppBundle\Entity\Questions */
            $question = $questions->get($i);
            $quests_array[] = $question->toJson();
        }

        $all_response = array(
            'match_id' => $match->getId(),
            'questions' => $quests_array,
        );

        return new CorsResponse($all_response);
    }

    /**
     * @Route("/api/result/{match_id}")
     * @param Request $request
     */
    public function resultAction($match_id, Request $request)
    {
        $matchSession = $this->getDoctrine()->getRepository(MatchInfo::class);
        /* @var $match MatchInfo */
        $match = $matchSession->find($match_id);

        if (!$match)
        {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
        }

        if ($match->getStatus() != MatchInfo::$STATUS_RESULT_OUT)
        {
            throw new \Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException();
        }
        /* session required for getting correct answer not implemented yet */
        $correctAnsSession = $this->getDoctrine()->getRepository(\AppBundle\Entity\CorrectAnswer::class);
        /* session required for getting user answer currently not implemented  */
        $userAnswerSession = $this->getDoctrine()->getRepository(\AppBundle\Entity\UserAnswer::class);
        /* required for calculating points */
        $objOptSession = $this->getDoctrine()->getRepository(\AppBundle\Entity\ObjectiveOptions::class);
        /* @var $user \AppBundle\Entity\User */
        $user = LoginController::isLoggedIn($this);
        /*
         * find the user answers on the basis of match id and 
         * user id
         */
        /* TODO: Write code for querying the user answers based on user and match | DONE */
        $userAnswers = $userAnswerSession->findBy(array(
            'matchId' => $match_id,
            'userId' => $user->getId(),
        ));

        /* calculated ponts  are going to store here */
        $points = 0;


        /* @var $oneAns \AppBundle\Entity\UserAnswer */
        foreach ($userAnswers as $oneAns)
        {
            /**
             * find the right answers on the basis of quetionId
             * and match ID as well 
             */
            /* TODO: Write code for querying the correct answers based on user answer | DONE */
            /* @var $oneAns \AppBundle\Entity\CorrectAnswer */
            $correctAns = $correctAnsSession->findOneBy(array(
                'qNo' => $oneAns->getQNo() ,
                'matchId' => $match_id
            ));
            
            if ($correctAns->getAnswer() === $oneAns->getAnswer())
            {
                /* @var $objOpt \AppBundle\Entity\ObjectiveOptions */
                $objOpt = $objOptSession->find($correctAns->getAnswer());
                $points += $objOpt->getExtraPoints();
            }
        }

        $result = array(
            'match_id' => $match_id,
            'total_points' => $points,
        );
        return new CorsResponse($result);
    }

    /**
     * Api to get matches played by current user
     * 
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/played-matches")
     */
    public function playedMatch(Request $request)
    {
//        $userId = 1;
////        
//        $userSession = $this->getDoctrine()->getRepository(\AppBundle\Entity\User::class);
//        /* @var $foundUser \FOS\UserBundle\Entity\User */
//        $foundUser = LoginController::isLoggedIn($this);

        /* @var $realUser \AppBundle\Entity\User */
        $realUser = LoginController::isLoggedIn($this);

        $matches = $realUser->getMatches();
        /**
         * empty array for storing the results
         * do not initialize it with some value
         * it is an empty array. 
         */
        /* @var $result array  */
        $result = array();

        for ($i = 0; $i < $matches->count(); $i++)
        {
            /* @var $match MatchInfo */
            $match = $matches->get($i);
            $result [] = $match->toJson();
        }

        return new CorsResponse($result);
    }

    /**
     * Ongoing routine for submitting answers
     * @param Request $request
     */
    public function submitAnswers(Request $request)
    {
        $matchId = 1;
        $matchSession = $this->getDoctrine()->getRepository(MatchInfo::class);
        $entityManager = $this->getDoctrine()->getEntityManager();
        $match = $matchSession->find($matchId);
        if (!$match)
        {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException("Match Not found !!");
        }

        $user = LoginController::isLoggedIn($this);

        if (!$user)
        {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User not found');
        }
        // add matches to user match list
        $user->getMatches()->add($match);
        // save it to database
        $entityManager->persist($user);
        $entityManager->flush();
    }

    public function todaysMatch(Request $request)
    {
        
    }

//    public function getJsonResponse($data)
//    {
//        $res = new \Symfony\Component\HttpFoundation\JsonResponse($data);
//        
//        $res->headers->set('Access-Control-Allow-Headers', 'origin, content-type, accept');
//        $res->headers->set('Access-Control-Allow-Origin', '*');
//        $res->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE OPTIONS');
//        return $res;
//    }
//    

    /**
     * @Route("/api/all-matches")
     * @param Request $request
     */
    public function getAllMatch(Request $request)
    {
        $entityManager = $this->getDoctrine()->getEntityManager();
        $query = $entityManager->createQuery(
                'select m from'
                .' AppBundle\Entity\MatchInfo m '
                . ' where m.createdOn < :today '
                . ' and m.endTime > :today '
                . ' and m.isPublished = 1 '
        )->setParameter('today', new \DateTime());
        
       
        
        
        
        /* @var $allMatch \Doctrine\Common\Collections\ArrayCollection */
        $allMatch = $query->execute(); //$repo->findAll();

        $matchesArray = array();
        
        foreach( $allMatch as $m  )
        {
            $matchesArray [] = $m->toJson();
        }


        return new CorsResponse($matchesArray);
    }

}
