<?php
namespace AppBundle\Controller;
class CorsResponse  extends \Symfony\Component\HttpFoundation\JsonResponse
{
    public function __construct($data = null, $status = 200, $headers = array())
    {
        parent::__construct($data, $status, $headers);
        $this->headers->set('Access-Control-Allow-Headers', 'origin, content-type, accept');
        $this->headers->set('Access-Control-Allow-Origin', '*');
        $this->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE OPTIONS');
    }
}