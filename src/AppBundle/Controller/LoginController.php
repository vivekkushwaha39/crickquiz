<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Controller\CorsResponse;

class LoginController extends Controller
{

    /**
     * @Route("/noapi/login")
     * @Template()
     */
    public function loginAction(\Symfony\Component\HttpFoundation\Request $request)
    {
        $userName = $request->get("user");
        $password = $request->get("password");

        // Retrieve the security encoder of symfony
        $factory = $this->get('security.encoder_factory');

        /// Start retrieve user
        // Let's retrieve the user by its username:
        // If you are using FOSUserBundle:
        $user_manager = $this->get('fos_user.user_manager');
        $user = $user_manager->findUserByUsername($userName);

        // Check if the user exists !
        if (!$user)
        {
            return new Response(
                    'Username doesnt exists', 404, array('Content-type' => 'application/json')
            );
        }

        /// Start verification
        $encoder = $factory->getEncoder($user);
        $salt = $user->getSalt();

        if (!$encoder->isPasswordValid($user->getPassword(), $password, $salt))
        {
            return new Response(
                    'Username or Password not valid.', 500, array('Content-type' => 'application/json')
            );
        }
        /// End Verification
        // The password matches ! then proceed to set the user in session
        //Handle getting or creating the user entity likely with a posted form
        // The third parameter "main" can change according to the name of your firewall in security.yml
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.context')->setToken($token);

        // If the firewall name is not main, then the set value would be instead:
        // $this->get('session')->set('_security_XXXFIREWALLNAMEXXX', serialize($token));
        $this->get('session')->set('_security_main', serialize($token));

        // Fire the login event manually
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        /*
         * Now the user is authenticated !!!! 
         * Do what you need to do now, like render a view, redirect to route etc.
         */
        return new CorsResponse(array(
            'name' => $user->getUsername(),
        ));
    }

    /**
     * @Route("/noapi/register")
     * @Template()
     */
    public function registerAction(\Symfony\Component\HttpFoundation\Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $entityManager = $this->get('doctrine')->getManager();
        $data = $request->request->all();

        // Do a check for existing user with userManager->findByUsername

        $user = $userManager->createUser();
        $user->setUsername($data['username']);

        $user->setPlainPassword($data['password']);
        $user->setEmail($data['email']);
        $user->setEnabled(true);

        $userManager->updateUser($user);

        return $this->generateToken($user, 201);
    }

    protected function generateToken($user, $statusCode = 200)
    {
//        $token = $this->get('lexik_jwt_authentication.jwt_manager')->create($user);

        $response = array(
//            'token' => $token,
            'user' => $user // Assuming $user is serialized, else you can call getters manually
        );

        return new CorsResponse($response, $statusCode); // Return a 201 Created with the JWT.
    }

    /**
     * @Route("/api/getuser")
     * @param \Symfony\Component\HttpFoundation\Request $req
     * @return Response
     */
    public function loggeduser(\Symfony\Component\HttpFoundation\Request $req)
    {
        /* @var $user \AppBundle\Entity\User */
        $user = $this->get('security.context')->getToken()->getUser();
        return new CorsResponse(array($user->getEmail()));
    }

    /**
     * 
     * @param Controller $context
     * @return Response
     */
    public static function isLoggedIn($context)
    {
        /* @var $user \AppBundle\Entity\User */
        $user = $context->get('security.context')->getToken()->getUser();

        return $user;
    }

}
