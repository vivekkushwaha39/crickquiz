<?php
namespace AppBundle\Handler;
use \Symfony\Component\Security\Http\Authentication as Auths;
class AuthHandler implements Auths\AuthenticationSuccessHandlerInterface,
 Auths\AuthenticationFailureHandlerInterface
{
    public function onAuthenticationSuccess(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
         $result = array('success' => false);
         return new Response(json_encode($result));
    }

    public function onAuthenticationFailure(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\Security\Core\Exception\AuthenticationException $exception)
    {
        throw new Exception('haha');
    }

}